#include <Servo.h>
#include <Wire.h>
#include <SparkFun_APDS9960.h>

// Global Variables
SparkFun_APDS9960 apds = SparkFun_APDS9960();
uint16_t ambient_light = 0;
uint16_t red_light = 0;
uint16_t green_light = 0;
uint16_t blue_light = 0;

int color[] = {160, 1, 2, 4, 4, 3, 4, 2, 4, 2};
int cycle = 0;

Servo myservo;
Servo bottom;
int pos = 0;

void setup() {
  myservo.attach(9, 544, 2000);
  bottom.attach(8, 544, 2000);
  // Initialize Serial port
  Serial.begin(9600);
  Serial.println();
  Serial.println(F("--------------------------------"));
  Serial.println(F("SparkFun APDS-9960 - ColorSensor"));
  Serial.println(F("--------------------------------"));
  
  // Initialize APDS-9960 (configure I2C and initial values)
  if ( apds.init() ) {
    Serial.println(F("APDS-9960 initialization complete"));
  } else {
    Serial.println(F("Something went wrong during APDS-9960 init!"));
  }
  
  // Start running the APDS-9960 light sensor (no interrupts)
  if ( apds.enableLightSensor(false) ) {
    Serial.println(F("Light sensor is now running"));
  } else {
    Serial.println(F("Something went wrong during light sensor init!"));
  }
  
  // Wait for initialization and calibration to finish
  delay(500);
}

void loop() {
  for (pos = 160; pos >= 95; pos -= 1) 
  { 
    myservo.write(pos);
    delay(20);              
  }
  delay(1000);
  // Read the light levels (ambient, red, green, blue)
  if (  !apds.readAmbientLight(ambient_light) ||
        !apds.readRedLight(red_light) ||
        !apds.readGreenLight(green_light) ||
        !apds.readBlueLight(blue_light) ) {
    Serial.println("Error reading light values");
  } 
  else 
  {
    Serial.print("Ambient: ");
    Serial.print(ambient_light);
    Serial.print(" Red: ");
    Serial.print(red_light);
    Serial.print(" Green: ");
    Serial.print(green_light);
    Serial.print(" Blue: ");
    Serial.println(blue_light);
  }

  if(green_light >= 20)
  {
    bottom.write(0);
  }
  else if(blue_light >= 20)
  {
    bottom.write(45);
  }
  else if(red_light >= 20)
  {
    bottom.write(135);
  }
  else
  {
    bottom.write(90);
  }
  delay(500);
  for (pos = 95; pos >= 0; pos -= 1) 
  { 
    myservo.write(pos);
    delay(20);              
  }
  delay(1000);
  for (pos = 0; pos <= 160; pos += 1)
  {
    myservo.write(pos);              
    delay(20); 
  }
  delay(1000);
}
