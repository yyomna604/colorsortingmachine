
#include <SPI.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <Adafruit_I2CDevice.h>
#define TFT_CS        10
#define TFT_RST        8 // Or set to -1 and connect to Arduino RESET pin
#define TFT_DC         9

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

#define RGB(r, g, b) (((r&0xF8)<<8)|((g&0xFC)<<3)|(b>>3))

// color definitions
#define  Black        RGB(0, 0, 0)
#define  Blue         RGB(0, 79, 255)
#define  Red          RGB(238, 1, 0)
#define  Green        RGB(0, 161, 0)
#define  Orange       RGB(255, 154, 0)
#define  Brown        RGB(101, 32, 0)
#define  Yellow       RGB(255,252,0)
#define  White        RGB(255,255,255)

void DrawAxes() {
  tft.fillScreen(White); //Color of the screen

  //Lines
  for (int i=0;i<8;++i) {
    tft.drawFastHLine(8,12+12*i,114,Black);
  }

  //Y-label text
  tft.setCursor(1,16);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("30");

  tft.setCursor(1,28);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("25");

  tft.setCursor(1,40);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("20");
  
  tft.setCursor(1,52);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("15");

  tft.setCursor(1,64);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("10");

  tft.setCursor(1,76);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("5");

  tft.setCursor(1,88);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("0");

  //Title
  tft.setCursor(10,2);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print("Sorting Statistics");
}

void TFT_Display(int Red_num,int Yellow_num,int Orange_num,int Green_num,int Brown_num,int Blue_num, int Unknown_num) {
  //Red rectangle
  tft.fillRect(18,96-round(12/5*Red_num),10,round(12/5*Red_num),Red);
  tft.drawRect(18,96-round(12/5*Red_num),10,round(12/5*Red_num),Black);
  tft.setCursor(20,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Red_num);

  //Yellow rectangle
  tft.fillRect(33,96-round(12/5*Yellow_num),10,round(12/5*Yellow_num),Yellow);
  tft.drawRect(33,96-round(12/5*Yellow_num),10,round(12/5*Yellow_num),Black);
  tft.setCursor(35,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Yellow_num);

  //Orange rectangle
  tft.fillRect(48,96-round(12/5*Orange_num),10,round(12/5*Orange_num),Orange);
  tft.drawRect(48,96-round(12/5*Orange_num),10,round(12/5*Orange_num),Black);
  tft.setCursor(50,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Orange_num);

  //Green rectangle
  tft.fillRect(63,96-round(12/5*Green_num),10,round(12/5*Green_num),Green);
  tft.drawRect(63,96-round(12/5*Green_num),10,round(12/5*Green_num),Black);
  tft.setCursor(65,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Green_num);

  //Brown rectangle
  tft.fillRect(78,96-round(12/5*Brown_num),10,round(12/5*Brown_num),Brown);
  tft.drawRect(78,96-round(12/5*Brown_num),10,round(12/5*Brown_num),Black);
  tft.setCursor(80,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Brown_num);

  //Blue rectangle
  tft.fillRect(93,96-round(12/5*Blue_num),10,round(12/5*Blue_num),Blue);
  tft.drawRect(93,96-round(12/5*Blue_num),10,round(12/5*Blue_num),Black);
  tft.setCursor(95,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Blue_num);

  //White rectangle
  tft.drawRect(108,96-round(12/5*Unknown_num),10,round(12/5*Unknown_num),Black);
  tft.setCursor(110,102);
  tft.setTextColor(Black);
  tft.setTextSize(1);
  tft.print(Unknown_num);
}

void setup(void) {
  tft.initR(INITR_BLACKTAB); // Init ST7735R chip, green tab
  //tft.fillScreen(ST77XX_BLACK);
  tft.setRotation(2); // set display orientation
  DrawAxes();
}

void loop() {
  TFT_Display(5,3,4,6,7,5,9);  
}
